from setuptools import setup

setup(
    name='po_client',
    py_modules=['po_client'],
    install_requires=[
        'requests'
    ]
)
