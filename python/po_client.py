import json
import time
import base64
import hmac
import hashlib
import requests


class AppId:
    CASH_REGISTER = 4


class POClient:
    def __init__(self, host, access_key, secret_key):
        self.host = host
        self.access_key = access_key
        self.secret_key = secret_key

        self.token = None
        self.session_id = None

    def sign_in(self, password, app_id=AppId.CASH_REGISTER, **params):
        res = self.request("post", "/apps/any/sessions", password=password, app_id=app_id, **params)

        if res.code != 200:
            raise Exception("POClient: authentication failed")

        self.token = res.json["token"]
        self.session_id = res.json["id"]

        return res

    def sign_out(self):
        return self.request("delete", "/apps/any/sessions/" + str(self.session_id))

    def request(self, method, path, use_json=False, **params):
        timestamp = self.timestamp()
        signature = self.signature(path, timestamp)

        params.update({"accesskey": self.access_key, "timestamp": timestamp, "signature": signature})

        if self.token:
            params["session"] = self.token

        data = {}

        if method not in ["get", "delete"]:
            params, data = data, params

        headers = {"Content-Type": "application/x-www-form-urlencoded", "Accept": "*/*"}

        req_params = dict(params=params, headers=headers, data=data)

        if use_json:
            headers['Content-Type'] = 'application/json; charset=utf-8'
            return POClientResponse(
                getattr(requests, method)(
                    self.host + path, json=dict(data, **params), headers=headers, timeout=120, allow_redirects=False
                )
            )

        return POClientResponse(getattr(requests, method)(self.host + path, **req_params))

    def timestamp(self):
        return time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime())

    def signature(self, path, timestamp):
        return base64.b64encode(
            hmac.new(self.secret_key.encode(), (path + timestamp).encode(), hashlib.sha256).digest()
        ).decode()


class POClientResponse:
    def __init__(self, res):
        self.code = res.status_code

        try:
            self.json = res.json()
        except:
            self.json = {}

        self.content = res.content
