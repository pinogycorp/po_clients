<?php

include_once './po_client.php';

date_default_timezone_set('UTC');

$po_client = new POClient('http://api-corepos-v1.pinogy.com', 'REPLACE_WITH_ACCESSKEY', 'REPLACE_WITH_SECRET_KEY');

$po_client->sign_in('REPLACE_WITH_PASSWORD', ["app_id" => 3]);
// Common app id's are below.  All app id's can be queried from the API using read__tbl__app_name
//  app_id |          app_name
//--------+---------------------------
//      4 | Cash Register
//      8 | Employee Dashboard
//     11 | House Accounts
//      3 | Pet Tracker
//      5 | Products
//      6 | Purchasing and Receiving
//      1 | Settings and Configuration

$res = $po_client->request('get', '/apps/any/imports');
// Another PHP example that passes a parameter is 
// $res = $po_client->request('post', '/apps/pet_tracker/queries/read__qpt__pet_info', ["qp_pet_status_id" => -1]);
print_r($res->code);
print_r($res->json);

$po_client->sign_out();

?>
