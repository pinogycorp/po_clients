import 'package:po_client/po_client.dart';

late POClient poClient;

Future<Map<String, dynamic>> pay(int txnId, num amount) async {
  Map<String, dynamic> payments = {
    "payments": [
      {"txnpay_amount": "$amount", "txnpay_payment_method_id": -1}
    ],
  };
  final resp =
      await poClient.request(method: "post", resource: "/api/v1/txns/$txnId/payments", params: payments, useJSON: true);
  return resp.data;
}

Future<Map<String, dynamic>> createLine(int txnId, int productId, {int qty = 24}) async {
  Map<String, dynamic> args = {
    "txnlines": [
      {
        "txnline_product_id": productId,
        "txnline_qty": qty,
      }
    ]
  };

  final resp =
      await poClient.request(method: "post", resource: "/api/v1/txns/$txnId/txnlines", params: args, useJSON: true);
  return resp.data;
}

Future<Map<String, dynamic>> closeTxn(int txnId) async {
  var resp = await poClient.request(
      method: "put", resource: "/api/v1/txns/$txnId", params: {"txn_status": "Closed"}, useJSON: true);
  return resp.data;
  // assert(resp.data["txn_status"] == "Closed", "Not closed");
}

Future<Map<String, dynamic>> createTxn(int customerId) async {
  final Map<String, dynamic> data = {
    "customer": {"entity_id": customerId},
    "txn_type": "Sale",
  };

  var resp = await poClient.request(method: "post", resource: "/api/v1/txns", params: data, useJSON: true);
  return resp.data;
}

Future<Map<String, dynamic>> readTxn(int txnId) async {
  var resp = await poClient.request(method: "GET", resource: "/api/v1/txns/$txnId", useJSON: true);
  return resp.data;
}

Future<void> createAndCloseTxn(int customerId, int productId) async {
  var resp1 = await createTxn(customerId);
  print("createTxn id=${resp1["transaction_id"]} total=${resp1["txn_totals"]["total"]}");
  var resp2 = await createLine(resp1["transaction_id"] as int, productId);
  print("createLine");
  var resp3 = await readTxn(resp1["transaction_id"] as int);
  print("readTxn total=${resp3["txn_totals"]["total"]}");
  final totalAmount =  num.parse(resp3["txn_totals"]["total"]);
  var resp4 = await pay(resp1["transaction_id"] as int, totalAmount);
  // var resp4 = await closeTxn(resp1["transaction_id"] as int);
  // print("closeTxn total=${resp3["txn_status"]}");
  resp3 = await readTxn(resp1["transaction_id"] as int);
  print("readTxn total=${resp3["txn_status"]}");
}

void main() async {
  final host = "https://api-corepos-v1-dev.pinogy.net";
  final accessKey = "XBd9Mn3vP9PzJBpVfGvV";
  final secretKey = "dkdvNCvFd87GDb9K4ZJS";

  poClient = POClient(host: host, accessKey: accessKey, secretKey: secretKey);
  poClient.configureLogging();
  await poClient.signIn(password: "Admin");
  await createAndCloseTxn(6130, 95362);
  await poClient.signOut();
}
