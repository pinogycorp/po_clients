import 'dart:convert';
import 'dart:io';
import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart' as dio;
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'logger.dart';

enum AppId {
  sac(1),
  sw(2),
  pt(3),
  cr(4),
  prod(5),
  par(6),
  ed(8),
  cd(9),
  hac(11);

  final int value;

  const AppId(this.value);
}

enum Language {
  en("en_US");

  final String value;

  const Language(this.value);
}

class POClientApiException implements Exception {
  late final dio.Response response;
  late final String errorMessage;
  late final String errorCode;

  POClientApiException(this.response) {
    try {
      errorCode = response.data["error"] ?? "ERR_GENERIC_CODE";
    } catch (_) {
      errorCode = "ERR_GENERIC_CODE";
    }
    try {
      errorMessage = response.data["message"] ?? "Error $httpCode";
    } catch (_) {
      errorMessage = "Error $httpCode";
    }
  }

  int get httpCode => response.statusCode!;
}

class POClient {
  final String accessKey;
  final String secretKey;
  final String host;
  final log = Logger('POClient');
  late final dio.Dio client;

  int? sessionId;
  String? token;

  POClient({required this.accessKey, required this.secretKey, required this.host, String? userAgent, dio.Dio? client}) {
    this.client = client ?? dio.Dio(dio.BaseOptions(
        baseUrl: host,
        headers: {"User-Agent": userAgent ?? "POClient ${Platform.operatingSystem}(dart)"},
        validateStatus: (status) => true,
        receiveDataWhenStatusError: true,
        connectTimeout: const Duration(seconds: 20),
        receiveTimeout: const Duration(seconds: 120)));
  }

  Future<Map<String, dynamic>> signIn(
      {required String password,
      AppId appId = AppId.cr,
      Language language = Language.en,
      String? version,
      Map<String, dynamic>? params}) async {
    final Map<String, dynamic> params = {
      "password": password,
      "app_id": appId.value,
      "language": language.value,
      "os": "${Platform.operatingSystem}-${Platform.operatingSystemVersion}"
    };

    if (version != null) {
      params["version"] = version;
    }
    var resp = await request(method: "POST", resource: "/apps/any/sessions", params: params);
    if (resp.statusCode != HttpStatus.ok) {
      throw POClientApiException(resp);
    }
    token = resp.data["token"];
    sessionId = resp.data["id"];
    return resp.data;
  }

  Future<void> signOut() async {
    await request(method: "DELETE", resource: "/apps/any/sessions/$sessionId");
    token = null;
    sessionId = null;
  }

  Future<dio.Response> request(
      {String method = "GET", required String resource, Map<String, dynamic>? params, useJSON = false}) async {
    final timestamp = "${DateTime.now().toUtc().toIso8601String().split(".")[0]}Z";
    final signature = getSignature(resource, timestamp);
    Map<String, dynamic> data = {"accesskey": accessKey, "timestamp": timestamp, "signature": signature};
    if (params != null) {
      data.addAll(params);
    }

    if (token != null) {
      data["session"] = token!;
    }

    final response = await client.request(resource,
        data: useJSON ? data : null,
        queryParameters: useJSON ? null : data,
        options: dio.Options(
            method: method,
            contentType: useJSON ? dio.Headers.jsonContentType : dio.Headers.formUrlEncodedContentType));
    return response;
  }

  String getSignature(String resource, String timestamp) {
    return base64.encode(Hmac(sha256, utf8.encode(secretKey)).convert(utf8.encode("$resource$timestamp")).bytes);
  }

  void configureLogging() {
    Logger.root.level = Level.FINE;
    Logger.root.onRecord.listen((LogRecord record) {
      final time = DateFormat("yyMMdd HH:mm:ss").format(record.time);
      print('[${record.level.name[0]} $time] ${record.message}');
    });
    client.interceptors.add(POClientLogInterceptor(logPrint: log.fine));
  }
}
