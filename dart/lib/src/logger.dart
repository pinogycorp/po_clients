import 'package:dio/dio.dart' as dio;

class POClientLogInterceptor extends dio.Interceptor {
  POClientLogInterceptor({
    required this.logPrint,
  });

  void Function(Object object) logPrint;

  @override
  void onRequest(
    dio.RequestOptions options,
    dio.RequestInterceptorHandler handler,
  ) {
    options.extra["requestStart"] = Stopwatch()..start();
    logPrint(
        "REQUEST ${options.method},${options.uri},body:${options.data} requestParameters${options.queryParameters}");
    handler.next(options);
  }

  @override
  void onResponse(dio.Response response, dio.ResponseInterceptorHandler handler) {
    Stopwatch timer = response.requestOptions.extra["requestStart"] as Stopwatch;
    timer.stop();
    logPrint(
        "RESPONSE code:${response.statusCode},time:${timer.elapsedMilliseconds / 1000},"
            "url:${response.realUri},body:${response.toString()}");
    handler.next(response);
  }
}
