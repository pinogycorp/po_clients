import 'dart:io';
import 'package:po_client/po_client.dart';
import 'package:test/test.dart';
import 'package:dio/dio.dart';

class MockDio with DioMixin implements Dio {
  final Response fakeResponse;

  MockDio(this.fakeResponse);

  @override
  Future<Response<T>> request<T>(
    String url, {
    Object? data,
    Map<String, dynamic>? queryParameters,
    CancelToken? cancelToken,
    Options? options,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    return fakeResponse as Response<T>;
  }

  noSuchMethod(Invocation invocation) {
    super.noSuchMethod(invocation); // Will throw.
  }
}

void main() {
  test("signature", () {
    var client = POClient(accessKey: "key", secretKey: "skey", host: "https://api.pinogy.com");
    final timestamp = "2025-01-01T20:00:00Z";
    var signature = client.getSignature("/api", timestamp);
    expect(signature, "aclqXwlVmYEGWXzcoI+RadauE1vx0NucrKFD+619+9w=");
  });

  test("login successful", () async {
    final session = {"id": 638957, "token": "ljkIKrolRko/l3twD9qNQ1HZUgtkRg=="};
    final expectedResponse = Response(
      data: session,
      requestOptions: RequestOptions(),
      statusCode: HttpStatus.ok,
      statusMessage: "OK",
    );
    final client = POClient(
        accessKey: "key", secretKey: "skey", host: "https://api.pinogy.com", client: MockDio(expectedResponse));
    final resp = await client.signIn(password: "secret");
    expect(client.sessionId, session["id"]);
    expect(client.token, session["token"]);
    expect(resp, session);
  });

  test("login incorrect", () async {
    // HttpStatus.notAcceptable:
    // "ERR_LOGIN_INCORRECT", "The provided password is incorrect."
    // "ERR_MISSING_PASSWORD", "A password is required to start a login session."
    // "ERR_APP_NOT_ACCESSIBLE", "Application is not accessible for the user"
    // "ERR_LOC_NOT_ACCESSIBLE", "Location is not accessible for the user"
    // HttpStatus.forbidden:
    // "ERR_LOGIN_INACTIVE", "The provided user is inactive."
    final errorResponse = {"error": "ERR_LOGIN_INCORRECT", "message": "Login incorrect"};
    final expectedResponse = Response(
      data: errorResponse,
      requestOptions: RequestOptions(),
      statusCode: HttpStatus.notAcceptable,
      statusMessage: "Not Acceptable",
    );
    var client = POClient(
        accessKey: "key", secretKey: "skey", host: "https://api.pinogy.com", client: MockDio(expectedResponse));
    var throwingFuture = client.signIn(password: "secret");
    await expectLater(
        throwingFuture,
        throwsA(isA<POClientApiException>()
            .having((e) => e.errorCode, "error", errorResponse["error"])
            .having((e) => e.errorMessage, "message", errorResponse["message"])));
  });
  test("login internal server error", () async {
    final expectedResponse = Response(
      data: "<html>Internal Server Error 500</html>",
      requestOptions: RequestOptions(),
      statusCode: HttpStatus.internalServerError,
      statusMessage: "Internal Server Error",
    );
    var client = POClient(
        accessKey: "key", secretKey: "skey", host: "https://api.pinogy.com", client: MockDio(expectedResponse));
    var throwingFuture = client.signIn(password: "secret");
    await expectLater(
        throwingFuture,
        throwsA(isA<POClientApiException>()
            .having((e) => e.errorCode, "error", equals("ERR_GENERIC_CODE"))
            .having((e) => e.errorMessage, "message", "Error 500")));
  });
}
