
#include "poClient.h"


std::string POClient::HttpMethodToString(HttpMethod method)
{
	switch (method) {
	case HttpMethod::GET:
		return "GET";
	case HttpMethod::POST:
		return "POST";
	case HttpMethod::DEL:
		return "DELETE";
	default:
		return "";
	}
}

std::string POClient::urlEncode(const std::string& input) {
	std::ostringstream escaped;
	escaped.fill('0');
	escaped << std::hex;
	for (char c : input) {
		if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
			escaped << c;
		}
		else if (c == ' ') {
			escaped << '+';
		}
		else {
			escaped << std::uppercase;
			escaped << '%' << std::setw(2) << static_cast<int>(static_cast<unsigned char>(c));
			escaped << std::nouppercase;
		}
	}
	return escaped.str();
}

std::string POClient::base64Encode(const unsigned char* buffer, size_t length)
{
	BIO* b64 = BIO_new(BIO_f_base64());
	BIO* bio = BIO_new(BIO_s_mem());
	BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
	BIO_push(b64, bio);
	BIO_write(b64, buffer, static_cast<int>(length));
	BIO_flush(b64);
	BUF_MEM* bptr;
	BIO_get_mem_ptr(b64, &bptr);
	std::string result(bptr->data, bptr->length - 1);  // Exclude the newline character
	BIO_free_all(b64);
	return urlEncode(result) + "=";
}

std::string POClient::GenerateSignature(const std::string& secretKey, const std::string& path, const std::string& timestamp)
{

	std::string data = path + timestamp;
	unsigned char hash[SHA256_DIGEST_LENGTH];
	HMAC(EVP_sha256(), secretKey.c_str(), secretKey.length(), reinterpret_cast<const unsigned char*>(data.c_str()), data.length(), hash, nullptr);
	return base64Encode(hash, SHA256_DIGEST_LENGTH);
}

std::string POClient::GenerateTimestamp()
{

	char buffer[96];
	time_t now;
	time(&now);
	struct tm now_gmt {};

	gmtime_s(&now_gmt, &now);

	strftime(buffer, sizeof(buffer),
		"%Y-%m-%dT%H:%M:%SZ", &now_gmt);
	return std::string(buffer);
}

std::string POClient::Request(HttpMethod method, const std::string& requestUri, std::string& response, const std::string& extraParameters)
{

	std::string url = host + requestUri;

	CURL* curl = curl_easy_init();
	if (curl) {
		struct curl_slist* headers = nullptr;
		std::string timestamp = GenerateTimestamp();
		std::string signature = GenerateSignature(secretKey, requestUri, timestamp);
		std::string authHeader = "accesskey=" + accessKey + "&timestamp=" + timestamp + "&signature=" + signature;

		// std::string params = extraParameters + "&timestamp=" + timestamp + "&signature=" + signature;
		std::string params;
		if (extraParameters == "")
		{
			params = authHeader;
		}
		else
		{
			params = extraParameters + "&" + authHeader;
		}

		if (method == HttpMethod::POST) {
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, params.c_str());
		}
		else {
			url += "?" + params;
		}

		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());

		// Set the callback function to handle the response
		std::string responseData;
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CurlCallback::WriteCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &responseData);

		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, HttpMethodToString(method).c_str());
		CURLcode res = curl_easy_perform(curl);
		if (res != CURLE_OK) {
			std::cerr << "Failed to perform HTTP request: " << curl_easy_strerror(res) << std::endl;
		}

		response = responseData;

		long responseCode;
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &responseCode);
		curl_slist_free_all(headers);
		curl_easy_cleanup(curl);
		return std::to_string(responseCode);
	}
	return "- 1";
}


//Sign In to get token
void POClient::SignIn(const std::string& password, int appId)
{
	std::string requestUri = SignInAddress;
	std::string responseData;

	std::string extraParameters = "password=" + password + "&app_id=" + std::to_string(appId);
	auto response = Request(HttpMethod::POST, requestUri, responseData, extraParameters);
	if (response == response_OK) {

		// Parse response
		json jsonData = json::parse(responseData);

		token = jsonData["token"];
		int session_id = jsonData["id"];
		sessionId = std::to_string(session_id);
	}
}

//Customer List
void POClient::CustomerList()
{
	std::string requestUri = CustomerAddress;
	std::string responseData;

	std::string extraParameters = "session=" + token + "&limit=2&offset=0";
	auto response = Request(HttpMethod::POST, requestUri, responseData, extraParameters);
	if (response == response_OK) {
		token.clear();
		sessionId.clear();
	}
}

//Product List
void POClient::ProductList()
{
	std::string requestUri = ProductAddress;
	std::string responseData;

	std::string extraParameters = "session=" + token + "&limit=2&qp_loc_entity_id=4";
	auto response = Request(HttpMethod::POST, requestUri, responseData, extraParameters);
	if (response == response_OK) {
		token.clear();
		sessionId.clear();
	}
}


//Product List
void POClient::LocationList()
{
	std::string requestUri = LocationAddress;
	std::string responseData;

	std::string extraParameters = "session=" + token + "&limit=2&offset=0";
	auto response = Request(HttpMethod::POST, requestUri, responseData, extraParameters);
	if (response == response_OK) {
		token.clear();
		sessionId.clear();
	}
}

//Transaction List
void POClient::TransactionList()
{
	std::string requestUri = TransactionAddress;
	std::string responseData;

	std::string extraParameters = "session=" + token + "&limit=2&order_by=txn_opened_on";
	auto response = Request(HttpMethod::GET, requestUri, responseData, extraParameters);
	if (response == response_OK) {
		token.clear();
		sessionId.clear();
	}
}

//Sign out
void POClient::SignOut()
{
	std::string requestUri = SignOutAddress + sessionId;
	std::string responseData;

	std::string extraParameters = "session=" + token;
	auto response = Request(HttpMethod::DEL, requestUri, responseData, extraParameters);
	if (response == response_OK) {
		token.clear();
		sessionId.clear();
	}
}


