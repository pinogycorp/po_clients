#pragma once

#include <iostream>
#include <sstream>
#include <iomanip>
#include <openssl/hmac.h>
#include <openssl/sha.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>
#include <curl/curl.h>
#include <time.h>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

class CurlCallback
{
public:
    // Callback function to write received data into a string
    static size_t WriteCallback(void* contents, size_t size, size_t nmemb, std::string* output)
    {
        size_t totalSize = size * nmemb;
        output->append(static_cast<char*>(contents), totalSize);
        std::cout << "Response Data: " << *output << std::endl;

        return totalSize;
    }
};

class POClient {
private:
    const std::string SignInAddress = "/apps/any/sessions";
    const std::string SignOutAddress = "/apps/any/sessions/";
    const std::string CustomerAddress = "/apps/any/queries/read__qpt__list_customers/";
    const std::string ProductAddress = "/apps/any/queries/read__qpt__list_products";
    const std::string LocationAddress = "/apps/any/queries/read__qpt__list_locations";
    const std::string TransactionAddress = "/api/v1/txns";
    const std::string response_OK = "200";

    std::string host;
    std::string accessKey;
    std::string secretKey;
    std::string token;
    std::string sessionId;

    enum class HttpMethod {
        GET,
        POST,
        DEL
    };

    std::string HttpMethodToString(HttpMethod method);
    std::string urlEncode(const std::string& input);
    std::string base64Encode(const unsigned char* buffer, size_t length);
    std::string GenerateSignature(const std::string& secretKey, const std::string& path,
        const std::string& timestamp);
    std::string GenerateTimestamp();
    std::string Request(HttpMethod method, const std::string& requestUri, std::string& response,
        const std::string& extraParameters = "");

public:
    POClient(const std::string& host, const std::string& accessKey, const std::string& secretKey)
        : host(host), accessKey(accessKey), secretKey(secretKey) {}

    void SignIn(const std::string& password, int appId);
    void CustomerList();
    void ProductList();
    void LocationList();
    void TransactionList();
    void SignOut();
};
