// posClientApplication.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "poClient.h"

int main()
{
    // Add the login credentials here
    std::string accessKey = "";
    std::string secretKey = "";
    std::string password = "";
    std::string api_url = "https://api-corepos-v1-dev.pinogy.net";
    int appId = 4;

    try
	{
		//NOTE: If you see any API cals fails while calling all togather here, call them one by one. 
		// Too many request from same
		// host might be blokcing calls.

		POClient client(api_url, accessKey, secretKey);

		std::cout << "\n============Calling Sign-in============\n\n";
		client.SignIn(password, appId);

		std::cout << "\n============Calling Customer List============\n\n";
		client.CustomerList();

		std::cout << "\n============Calling Product List============\n\n";
		client.ProductList();

		std::cout << "\n============Calling Location List============\n\n";
		client.LocationList();

		std::cout << "\n============Calling Transaction List============\n\n";
		client.TransactionList();

		std::cout << "\n============Calling Sign-out============\n\n";
		client.SignOut();
	}
    catch (const std::exception& ex) {
        std::cerr << "Error: " << ex.what() << std::endl;
    }

    std::cin.get();
    return 0;
}

