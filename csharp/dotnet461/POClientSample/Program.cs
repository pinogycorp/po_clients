﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace POClientSample
{
    public class Program
    {
        private static void Main(string[] args)
        {
            Run().Wait();
        }

        private static async Task Run()
        {
            var accessKey = "";
            var secretKey = "";
            var password = "";
            var appId = 0;

            try
            {
                HttpResponseMessage response;

                var client = new POClient("http://api-corepos-v1.pinogy.com", accessKey, secretKey);

                response = await client.SignInAsync(password, appId);

                await PrintResponseAsync(response);

                response = await client.RequestAsync(HttpMethod.Post, "/apps/pet_tracker/queries/read__qpt__pet_info", new Dictionary<string, string>
                {
                    { "qp_pet_status_id", "-1" }
                });

                await PrintResponseAsync(response);

                response = await client.RequestAsync(HttpMethod.Get, "/apps/any/imports");

                await PrintResponseAsync(response);

                response = await client.SignOutAsync();

                await PrintResponseAsync(response);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }

        private static async Task PrintResponseAsync(HttpResponseMessage response)
        {
            var content = await response.Content.ReadAsStringAsync();

            Console.WriteLine($"Request to {response.RequestMessage.RequestUri}");
            Console.WriteLine();
            Console.WriteLine(content);
            Console.WriteLine();
        }
    }
}
