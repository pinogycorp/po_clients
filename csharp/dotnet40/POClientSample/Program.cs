﻿using System;
using System.Collections.Generic;
using System.Net.Http;

namespace POClientSample
{
    public class Program
    {
        private static void Main(string[] args)
        {
            Run();
        }

        private static void Run()
        {
            var accessKey = "";
            var secretKey = "";
            var password = "";
            var appId = 0;

            try
            {
                HttpResponseMessage response;

                var client = new POClient("http://api-corepos-v1.pinogy.com", accessKey, secretKey);

                response = client.SignIn(password, appId);

                PrintResponse(response);

                response = client.Request(HttpMethod.Post, "/apps/pet_tracker/queries/read__qpt__pet_info", new Dictionary<string, string>
                {
                    { "qp_pet_status_id", "-1" }
                });

                PrintResponse(response);

                response = client.Request(HttpMethod.Get, "/apps/any/imports");

                PrintResponse(response);

                response = client.SignOut();

                PrintResponse(response);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }

        private static void PrintResponse(HttpResponseMessage response)
        {
            var content = response.Content.ReadAsStringAsync().Result;

            Console.WriteLine($"Request to {response.RequestMessage.RequestUri}");
            Console.WriteLine();
            Console.WriteLine(content);
            Console.WriteLine();
        }
    }
}
