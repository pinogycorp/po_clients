﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace POClientSample
{
    public class POClient
    {
        private const string SignInAddress = "/apps/any/sessions";
        private const string SignOutAddress = "/apps/any/sessions/{0}";

        private readonly string host;
        private readonly string accessKey;
        private readonly string secretKey;

        private string token;
        private string sessionId;

        public POClient(string host, string accessKey, string secretKey)
        {
            this.host = host;
            this.accessKey = accessKey;
            this.secretKey = secretKey;
        }

        public HttpResponseMessage SignIn(string password, int appId)
        {
            var requestUri = SignInAddress;

            var extraParameters = new Dictionary<string, string>
            {
                { "password", password },
                { "app_id", appId.ToString() }
            };

            var response = Request(HttpMethod.Post, requestUri, extraParameters);

            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;
                var signInResponse = JsonConvert.DeserializeObject<SignInResponse>(content);

                token = signInResponse.Token;
                sessionId = signInResponse.Id;
            }

            return response;
        }

        public HttpResponseMessage SignOut()
        {
            var requestUri = string.Format(SignOutAddress, sessionId);
            var response = Request(HttpMethod.Delete, requestUri);

            if (response.IsSuccessStatusCode)
            {
                token = null;
                sessionId = null;
            }

            return response;
        }

        public HttpResponseMessage Request(HttpMethod httpMethod, string requestUri, IDictionary<string, string> extraParameters = null)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(host)
            };

            var timestamp = GenerateTimestamp();
            var signature = GenerateSignature(secretKey, requestUri, timestamp);

            var parameters = new Dictionary<string, string>(extraParameters ?? new Dictionary<string, string>())
            {
                { "accesskey", accessKey },
                { "timestamp", timestamp },
                { "signature", signature },
                { "session", token }
            };

            if (httpMethod != HttpMethod.Post)
                requestUri = requestUri + "?" + string.Join("&", parameters.Select(param => param.Key + "=" + HttpUtility.UrlEncode(param.Value)));

            var request = new HttpRequestMessage(httpMethod, requestUri);

            request.Headers.Accept.ParseAdd("*/*");

            if (httpMethod == HttpMethod.Post)
                request.Content = new FormUrlEncodedContent(parameters);

            return client.SendAsync(request).Result;
        }

        private static string GenerateSignature(string secretKey, string requestUri, string timestamp)
        {
            var encoding = new ASCIIEncoding();
            var key = encoding.GetBytes(secretKey);
            var message = encoding.GetBytes(requestUri + timestamp);
            var hmac = new HMACSHA256(key);
            var hash = hmac.ComputeHash(message);
            var signature = Convert.ToBase64String(hash);

            return signature;
        }

        private static string GenerateTimestamp()
        {
            return DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }
    }

    public struct SignInResponse
    {
        public string Token;
        public string Id;
    }
}
